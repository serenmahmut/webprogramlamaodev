﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebApplication29.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class lang {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal lang() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("WebApplication29.Resources.lang", typeof(lang).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Anasayfa.
        /// </summary>
        public static string anasayfa {
            get {
                return ResourceManager.GetString("anasayfa", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Araştır.
        /// </summary>
        public static string arastir {
            get {
                return ResourceManager.GetString("arastir", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Dersler.
        /// </summary>
        public static string dersler {
            get {
                return ResourceManager.GetString("dersler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giriş Yap.
        /// </summary>
        public static string girisyap {
            get {
                return ResourceManager.GetString("girisyap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Haberler.
        /// </summary>
        public static string haberler {
            get {
                return ResourceManager.GetString("haberler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hakkımda.
        /// </summary>
        public static string hakkimda {
            get {
                return ResourceManager.GetString("hakkimda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İletişim.
        /// </summary>
        public static string iletisim {
            get {
                return ResourceManager.GetString("iletisim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SOSYAL MEDYA HESAPLARIM.
        /// </summary>
        public static string sosyal {
            get {
                return ResourceManager.GetString("sosyal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tüm Liste.
        /// </summary>
        public static string tümliste {
            get {
                return ResourceManager.GetString("tümliste", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Üye Ol.
        /// </summary>
        public static string üyeol {
            get {
                return ResourceManager.GetString("üyeol", resourceCulture);
            }
        }
    }
}
