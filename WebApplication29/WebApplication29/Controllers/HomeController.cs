﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication29.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your about page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Haberler()
        {
            ViewBag.Message = "Haberler Sayfası.";

            return View();
        }


        public ActionResult internet()
        {
            ViewBag.Message = "internet haber";

            return View();
        }

        public ActionResult iphone()
        {
            ViewBag.Message = "iphone haber";

            return View();
        }

        public ActionResult facebook()
        {
            ViewBag.Message = "facebook haber";

            return View();
        }

        public ActionResult uygulama()
        {
            ViewBag.Message = "uygulama haber";

            return View();
        }


        public ActionResult microsoft()
        {
            ViewBag.Message = "microsoft haber";

            return View();
        }

        public ActionResult vpn()
        {
            ViewBag.Message = "vpn haber";

            return View();
        }

        public ActionResult Htmlders()
        {
            ViewBag.Message = "Html Ders";

            return View();
        }



        public ActionResult Cssders()
        {
            ViewBag.Message = "Css Ders";

            return View();
        }




        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }



    }
}